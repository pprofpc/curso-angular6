import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destinos-favoritos',
  templateUrl: './destinos-favoritos.component.html',
  styleUrls: ['./destinos-favoritos.component.css']
})
export class DestinosFavoritosComponent implements OnInit {

favoritos: String [];

  constructor() {
    this.favoritos = ['Londres, Reino Unido','París, Francia', 'Roma, Italia','Creta, Grecia','Bali, Indonesia','Phuket, Tailandia'];
    }

  ngOnInit(): void {
  }

}
