import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinosFavoritosComponent } from './destinos-favoritos.component';

describe('DestinosFavoritosComponent', () => {
  let component: DestinosFavoritosComponent;
  let fixture: ComponentFixture<DestinosFavoritosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DestinosFavoritosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinosFavoritosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
