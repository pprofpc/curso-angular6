import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { DestinosViajesState } from './../models/destinos-viajes-state.model';
import { Store } from '@ngrx/store';
import { AppState } from './../app.module'

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  //destinos: DestinoViaje[];
  constructor(
    public destinosApiClient: DestinosApiClient, 
    private store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit() {
    this.store.select(state => state.destinos)
      .subscribe(data => {
        let d = data.favorito;
        if (d != null) {
          this.updates.push('Se ha elegido a ' + d.nombre)
        }
      });
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  
  /*
  guardar(nombre:String, url:String):boolean{
    this.destinos.push(new DestinoViaje(nombre, url));
    //console.log(this.destinos);
    return false;
  }
  */
 
  elegido(e: DestinoViaje) {
    //desmarcar todos los demas en en array de elegidos
    //this.destinos.forEach(function (x) {x.setSelected(false); });
    //se marca el elegido
    //d.setSelected(true);
    //this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    //e.setSelected(true);
    this.destinosApiClient.elegir(e);
  }
}
